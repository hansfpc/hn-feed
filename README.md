# Hacker News Feed
 [![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

A web application that shows most recent 'nodejs' related posts of Hacker News

![](screenshots/1.jpeg)


### Run the app

> Please open http://localhost to visualize front-end app in the browser

    $ docker-compose up

### Stop the app

    $ docker-compose down
