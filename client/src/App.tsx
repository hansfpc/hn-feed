import React from 'react'
import styled from 'styled-components'
import Header from './components/Header'
import Articles from './components/Articles'

const Container = styled.div`
  display: grid;
  grid-template: 150px 1fr / 1fr;
  background: #fff;
  height: 100vh;
`

const App = () => {
  return (
    <Container>
      <Header />
      <Articles />
    </Container>
  )
}

export default App
