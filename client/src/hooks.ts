import { useEffect, useState } from 'react'
import axios from 'axios'
import config from './config'

interface Article {
  hn_id: string
  author: string
  title: string
  url?: string | null
  date: string
}

export const getArticles = () => axios.get(`${config.HOST}/articles`)
export const removeArticle = (id: any) => axios.put(`${config.HOST}/articles/${id}/block`)

export function useArticles() {
  const [fetching, setFetching] = useState(false)
  const [removing, setRemoving] = useState(false)

  const [fetchError, setFetchError] = useState()
  const [removeError, setRemoveError] = useState()

  const [articles, setArticles] = useState<Article[]>([])

  async function onDelete(articleId: string) {
    setRemoving(true)
    try {
      await removeArticle(articleId)
      setArticles(articles.filter(article => article.hn_id !== articleId))
    } catch (e) {
      setRemoveError(e.message)
    }
    setRemoving(false)
  }

  useEffect(() => {
    async function onFetch() {
      setFetching(true)

      try {
        const result = await getArticles()
        setArticles(result.data)
      } catch (e) {
        setFetchError(e.message)
      }
      setFetching(false)
    }

    onFetch()
  }, [])

  return { articles, fetching, removing, fetchError, removeError, onDelete }
}
