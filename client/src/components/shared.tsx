import styled from 'styled-components'

export const Title = styled.span`
  font-size: 50px;
  font-weight: 900;
`

export const SubTitle = styled.span`
  font-size: 18px;
`

export const TitleLabel = styled.span`
  color: #333;
  font-size: 13pt;
`

export const AuthorLabel = styled(TitleLabel)`
  color: #999;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #f7f807;
  padding-left: 15px;
`
