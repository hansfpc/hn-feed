import React from 'react'
import styled from 'styled-components'
import { SubTitle, Title } from './shared'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #f7f807;
  padding-left: 15px;
`

const Header = () => {
  return (
    <Container>
      <Title>HN Feed</Title>
      <SubTitle>{'We <3 hacker news!'}</SubTitle>
    </Container>
  )
}

export default Header
