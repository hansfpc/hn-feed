import React from 'react'
import styled from 'styled-components'
import { AuthorLabel, TitleLabel } from './shared'

type Props = {
  title: string
  author: string
  date: string
  url: string
  onClick: (e: any) => void
}

const Container = styled.li`
  display: grid;
  grid-template: 1fr / 1fr 100px 30px;
  align-items: center;
  border-bottom: 1px solid #ccc;
  background-color: #fff;
  padding: 10px;
  :hover {
    background-color: #fafafa;
  }
`

const Link = styled.a`
  text-decoration: none;
  color: inherit;
  width: 100%;
`

const Button = styled.i`
  cursor: pointer;
  :hover {
    color: red;
  }
`

const Item = ({ title, author, date, url, onClick }: Props) => {
  return (
    <Link href={url} target="_blank">
      <Container>
        <div>
          <TitleLabel>{title}</TitleLabel>
          <AuthorLabel>{` - ${author} -`}</AuthorLabel>
        </div>
        <TitleLabel>{date}</TitleLabel>
        <Button className="fas fa-trash-alt" onClick={onClick} />
      </Container>
    </Link>
  )
}

export default Item
