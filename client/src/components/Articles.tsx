import React from 'react'
import styled from 'styled-components'
import Item from './Item'
import { useArticles } from '../hooks'

const Container = styled.div`
  margin: 10px 15px;
`

const List = styled.ul`
  background: #6a4188;
  display: grid;
  list-style-type: none;
  margin: 0;
  padding: 0;
`

export default () => {
  const { articles, fetching, onDelete, fetchError } = useArticles()

  if (fetching) return <h1>Loading...</h1>

  if (fetchError) return <h1>{fetchError}</h1>

  return (
    <Container>
      <List>
        {articles.map(item => (
          <Item
            key={item.hn_id}
            title={item.title}
            author={item.author}
            date={item.date}
            url={item.url || ''}
            onClick={e => {
              e.preventDefault()
              onDelete(item.hn_id)
            }}
          />
        ))}
      </List>
    </Container>
  )
}
