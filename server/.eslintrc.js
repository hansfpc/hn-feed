// @ts-ignore
module.exports = {
  env: { node: true, "jest": true },
  parser: '@typescript-eslint/parser',
  extends: [
    'airbnb',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  plugins: ['@typescript-eslint'],
  rules: {
    "import/extensions": ['error', "never"],
    "camelcase": "off",
    "@typescript-eslint/camelcase": ["error", { "properties": "never" }],
    "@typescript-eslint/interface-name-prefix": ['off', 'never'],
    'prettier/prettier': ['error', { singleQuote: true }],
    semi: ['error', 'never'],
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/no-explicit-any': 0,
    'import/no-unresolved': 0,
    'no-underscore-dangle': 0,
    'import/prefer-default-export': 0,
    'max-len': ['error', { code: 90, ignoreUrls: true }],
    '@typescript-eslint/no-var-requires': 0,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.ts', 'js'],
      },
    },
  },
}
