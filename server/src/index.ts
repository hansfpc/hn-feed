import http from 'http'
import express, { Application } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import databaseConnection from './config/db'
import hnCronJob from './scripts/HNCronJob'
import logger from './utils/logger'

import router from './routes'
import errors, { genError } from './utils/errors'

const PORT = 3001

const app: Application = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

router.map((route: any) => app.use(route.path, route.middleware, route.handler))

const server = http.createServer(app)

databaseConnection()
  .then(() => {
    server.listen(PORT)
    hnCronJob()
  })
  .catch((err: any) => {
    logger.error(genError(errors.CANNOT_CONNECT_DB, err.stack))
    process.exit(1)
  })

server.on('listening', () => logger.info(`Server is running at port ${PORT}`))
