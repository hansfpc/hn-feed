import { formatDate } from '../../utils/date'

import moment = require('moment')

describe('Date Utils', () => {
  it('should return date as Yesterday', () => {
    const rawDate = moment()
      .add(-1, 'days')
      .startOf('day')
    expect(formatDate(rawDate)).toEqual('Yesterday')
  })

  it('should return date in format HH:mm a', () => {
    const today = moment().startOf('day')
    expect(formatDate(today)).toEqual(moment(today).format('HH:mm a'))
  })

  it('should return date in format MMM DD', () => {
    const rawDate = moment()
      .add(-4, 'days')
      .startOf('day')
    expect(formatDate(rawDate)).toEqual(moment(rawDate).format('MMM DD'))
  })
})
