import errors, { genError } from '../../utils/errors'

describe('Error Utils', () => {
  it('should return NOT FOUND error', () => {
    const expectedError = {
      code: 404,
      title: 'RESOURCE NOT FOUND',
      msg: 'Resource not found',
    }
    expect(genError(errors.NOT_FOUND, 'Resource not found')).toEqual(expectedError)
  })
})
