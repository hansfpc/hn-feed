import { Request, Response } from 'express'
import * as ArticleService from '../services/Article'
import logger from '../utils/logger'
import errors, { genError } from '../utils/errors'

export const getArticles = async (req: Request, res: Response) => {
  try {
    const articles = await ArticleService.getAllArticles()
    return res.status(200).send(articles)
  } catch (err) {
    logger.error(genError(errors.NOT_FOUND, err.stack))
    return res.status(404).send(err.errors)
  }
}

export const blockArticle = async (req: Request, res: Response) => {
  const { id } = req.params
  try {
    const blockedArticle = await ArticleService.blockArticle(id)
    if (!blockedArticle) {
      logger.error(genError(errors.NOT_FOUND, 'Cannot get blocked article'))
      return res.status(404)
    }
    return res.status(200).send(blockedArticle)
  } catch (err) {
    logger.error(genError(errors.NOT_FOUND, err.stack))
    return res.status(404).send(err.errors)
  }
}
