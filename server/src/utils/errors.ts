interface CustomError {
  code: number
  title: string
  msg: any
}

export const errors = {
  NOT_FOUND: { code: 404, title: 'RESOURCE NOT FOUND', msg: undefined },
  CANNOT_CONNECT_DB: { code: 1001, title: 'Cannot connect to Database', msg: undefined },
}

export const genError = (error: any, stack: any): CustomError => {
  error.msg = stack
  return error
}

export default errors
