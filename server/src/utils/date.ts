import moment from 'moment'

export const formatDate = (date: any) => {
  const today = moment().startOf('day')
  const yesterday = moment()
    .add(-1, 'days')
    .startOf('day')

  if (moment(date).isSame(today, 'd')) return moment(date).format('HH:mm a')
  if (moment(date).isSame(yesterday, 'd')) return 'Yesterday'

  return moment(date).format('MMM DD')
}
