import express from 'express'
import { Article } from '../controllers'

const router = express.Router()

// get all articles list (formatted)
router.get('/', Article.getArticles)

// mark an article as blocked (used for "delete" purposes on front-end app)
router.put('/:id/block', Article.blockArticle)

export default router
