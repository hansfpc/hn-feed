import Article from './Article'

const routes = [
  {
    path: '/articles',
    middleware: [],
    handler: Article,
  },
]

export default routes
