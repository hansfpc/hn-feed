import axios from 'axios'
import Article, { IArticleModel } from '../models/Article'
import { formatDate } from '../utils/date'
import config from '../config'
import logger from '../utils/logger'

interface IRawArticleModel {
  // describes Article Model from HN API (not formatted)
  objectID: string
  author: string
  story_title: string
  title: string
  created_at: Date
  story_url: string
  url: string
}

export const getAllArticles = () => {
  return Article.find({ blocked: false })
    .sort({ date: -1 })
    .then(articles => {
      return articles.map((article: IArticleModel) => ({
        hn_id: article.hn_id,
        author: article.author,
        title: article.title,
        url: article.url,
        date: formatDate(article.date),
      }))
    })
}

export const blockArticle = (id: string) => {
  return Article.findOneAndUpdate({ hn_id: id }, { blocked: true }, { new: true }).then(
    article => {
      if (!article) return false
      return article
    }
  )
}

export const searchForArticles = async () => {
  const response = await axios.get(config.API.ENDPOINT)

  const hnArticles: IRawArticleModel[] = response.data.hits || []

  const feedArticles: any[] = []
  for (const article of hnArticles) {
    if (!article.story_title && !article.title) {
      return
    }
    const myArticle = {
      hn_id: article.objectID,
      title: article.story_title || article.title,
      date: article.created_at,
      url: article.story_url || article.url,
      author: article.author,
    }
    feedArticles.push(myArticle)
  }

  if (feedArticles.length !== 0) {
    try {
      Article.insertMany(feedArticles, { ordered: false }).then(() =>
        logger.info('Inserting articles (insertMany)')
      )
    } catch (err) {
      logger.error(err)
    }
  }
}
