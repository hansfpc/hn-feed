export default {
  API: {
    ENDPOINT: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
  },
  DB: {
    NAME: 'hackernews',
    HOST: process.env.DB_HOST || '127.0.0.1',
  },
  HN_CRONJOB: {
    TIME_RANGE: '0 0 */1 * * *',
  },
}
