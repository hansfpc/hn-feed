import Mongoose from 'mongoose'
import config from '.'

Mongoose.Promise = global.Promise

const databaseConnection = () =>
  Mongoose.connect(`mongodb://${config.DB.HOST}:27017/${config.DB.NAME}`, {
    useUnifiedTopology: true,
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })

export default databaseConnection
