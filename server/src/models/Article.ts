import Mongoose, { Document } from 'mongoose'

export interface IArticleModel extends Document {
  hn_id: string
  author: string
  title: string
  date: Date
  url: string
  blocked: boolean
}

const articleSchema = new Mongoose.Schema({
  hn_id: { type: String, unique: true },
  author: { type: String, trim: true },
  title: { type: String, trim: true },
  date: { type: String, default: Date.now.toString },
  url: { type: String, trim: true },
  blocked: { type: Boolean, default: false },
})

export default Mongoose.model<IArticleModel>('Article', articleSchema)
