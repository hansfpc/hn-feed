import { CronJob } from 'cron'
import { Article } from '../models'
import config from '../config'
import { searchForArticles } from '../services/Article'

export default async () => {
  const storedArticles = (await Article.find({}).countDocuments()) > 0

  if (!storedArticles) searchForArticles()
  const cronJob = new CronJob(config.HN_CRONJOB.TIME_RANGE, searchForArticles)
  cronJob.start()
}
