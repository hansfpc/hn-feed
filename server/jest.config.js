module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['src'],
  coverageThreshold: {
    global: {
      functions: 50,
    },
  },
  coverageReporters: ['json', 'lcovonly', 'text', 'clover'],
  collectCoverageFrom: ['src/**/*', '!**/node_modules/**'],
}
